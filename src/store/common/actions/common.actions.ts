import { createAction, props } from '@ngrx/store';

import { CheckState, CurrentFields } from "../types/interfaces";

export const state = createAction('[Common Component] State');
export const addCurrentNewField = createAction(
  '[Common Component] Update',
  props<{value: CurrentFields}>()
);
export const saveNewValue = createAction(
  '[Common Component] Save',
  props<{value:
      CurrentFields[],
    }>()
);
export const editNewValue = createAction(
  '[Common Component] Edit',
  props<{value: number}>()
);
export const removeField = createAction(
  '[Common Component] Remove',
  props<{value: number}>()
);
export const disableEdit = createAction(
  '[Common Component] DisableEdit'
);
export const addCheck = createAction(
  '[Common Component] AddCheck',
  props<{value: CheckState[] | CheckState}>()
);
export const setCurrentId = createAction(
  '[Common Component] SetCurrentId',
  props<{value: string | null}>()
);
export const setCurrent = createAction(
  '[Common Component] SetCurrentId',
  props<{value: CheckState}>()
);
export const clearCurrentState = createAction(
  '[Common Component] ClearCurrentState'
);
export const removeCheck = createAction(
  '[Common Component] RemoveCheck',
  props<{value: number | string}>()
);

