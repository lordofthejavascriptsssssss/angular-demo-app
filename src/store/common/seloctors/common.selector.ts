import { createFeatureSelector, createSelector } from '@ngrx/store';
// import { IndexStateInterface } from '../../app-types/state.interface';
import { CommonState } from "../types/interfaces";

export const indexFeatureSelector = createFeatureSelector<
  CommonState
  >('common');

export const getCommonCount = createSelector(
  indexFeatureSelector,
  (CommonState: CommonState) => CommonState.commonCount
);

export const getCommonCurrentCheck = createSelector(
  indexFeatureSelector,
  (CommonState: CommonState) => CommonState.currentCheckFields
);

export const getAllChecks = createSelector(
  indexFeatureSelector,
  (CommonState: CommonState) => CommonState.allCheck
);

export const getCurrentId = createSelector(
  indexFeatureSelector,
  (CommonState: CommonState) => CommonState.currentId
);

export const getCurrentTitle = createSelector(
  indexFeatureSelector,
  (CommonState: CommonState) => CommonState.currentCheckTitle
);
