import {Action, createReducer, createSelector, on} from '@ngrx/store';
import {
  state,
  addCurrentNewField,
  saveNewValue,
  editNewValue,
  disableEdit,
  removeField,
  addCheck,
  setCurrentId,
  setCurrent,
  clearCurrentState,
  removeCheck
} from './actions/common.actions';
import { CommonState, CurrentFields, CheckState  } from "./types/interfaces";
import { cloneDeep } from "lodash";
import {indexFeatureSelector} from "./seloctors/common.selector";


export const initialState = {
  commonCount: 0,
  commonString: 'string',
  currentCheckTitle: '',
  currentId: null as string | null,
  currentCheckFields: [] as CurrentFields[],
  allCheck: [] as CheckState[]
};

const _commonReducer = createReducer(
  initialState,
  on(clearCurrentState, (state) => {
    return {
      ...state,
      currentId: null,
      currentCheckTitle: '',
      currentCheckFields: []
    }
  }),
  on(state, (state) => {
    return state
  }),
  on(removeField, (state, {value}) => {
    return {
      ...state,
      currentCheckFields: cloneDeep(state.currentCheckFields)
        .filter((v)=> v.id !== value)
        .map((v, i) => {
          return {
            ...v,
            id: i,
            isEdit: false
          }
        })
    }
  }),
  on(disableEdit, (state) => {
    return {
      ...state,
      currentCheckFields: cloneDeep(state.currentCheckFields).map((v)=> {
        return {
          ...v,
          isEdit: false
        }
      })
    }
  }),
  on(editNewValue, (state, {value}) => {
    const result = cloneDeep(state.currentCheckFields).map((v) => {
      if(v.id === value){
        console.log(v.imageUrl, 'edit')
        return {
          ...v,
          isEdit: !v.isEdit
        }
      } else {
        return {
          ...v,
          isEdit: false
        }
      }
    })
    console.log(result, 'edit')
    return {
      ...state,
      currentCheckFields: result
    }
  }),
  on(saveNewValue, (state, { value }) => {
    console.log(value)
    const result = cloneDeep(value).map((v, i) => {
      const item = state.currentCheckFields.find((v) => v.id === i)
      if(item){
        // console.log(v, item, 'item')
        return {
          ...v,
          // imageUrl: item.imageUrl,
          id: item.id,
          isEdit: item.isEdit
        }
      }
      return {
        ...v,
        id:i,
        imageUrl: '',
        isEdit: false,
      }
    })
    return {
      ...state,
      currentCheckFields: result
    }
  }),
  on(addCurrentNewField, (state, { value } ) => {
    return {
      ...state,
      currentCheckFields: mapValue(state.currentCheckFields, value)
    }
  }),
  on(addCheck, (state, { value } ) => {
    return {
      ...state,
      allCheck: (Array.isArray(value)) ? value : mapValue(state.allCheck, value)
    }
  }),
  on(setCurrentId, (state, { value } ) => {
    return {
      ...state,
      currentId: value
    }
  }),
  on(setCurrent, (state, { value } ) => {
    if(!value){
      return {
        ...state
      }
    }
    return {
      ...state,
      currentId: value.id,
      currentCheckFields: value.listItems,
      currentCheckTitle: value.title
    }
  }),
  on(removeCheck, (state, { value } ) => {
    return {
      ...state,
      allCheck: state.allCheck.filter((item) => item.id !== value)
    }
  }),
);

function mapValue<T extends { id: string | number },>(state: T[], value: T): T[] {
  let isUnique = false
  const result = cloneDeep(state)
    .map((v,i,arr) => {
      if(v.id === value.id){
        v = value
      }
      if(i === state.length - 1){
        isUnique = !arr.map((v) => v.id).includes(value.id)
      }
      return v
    });
  if (isUnique || result.length === 0){
    result.push(value)
  }
  return result
}

export function commonReducer<T>(state: CommonState, action:Action) {
  return _commonReducer(state, action);
}
