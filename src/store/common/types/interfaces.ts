export interface CurrentFields {
  id: number,
  imageUrl: string,
  checkPriceName: string,
  isEdit: boolean,
  checkPriceValue: number
}

export interface CommonState {
  commonCount: number,
  commonString: string,
  currentCheckTitle: string,
  currentId: string | null,
  currentCheckFields: CurrentFields[] | []
  allCheck: CheckState[] | []
}

export interface CheckState {
  date: string,
  id: string,
  title: string,
  listItems: CurrentFields[]
}
