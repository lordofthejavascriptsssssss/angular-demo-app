import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import SwiperCore, { Navigation } from "swiper";
import { EventsParams } from 'swiper/angular';
import { MainSlideTypes } from "../../types/swiper.types";
// install Swiper modules
SwiperCore.use([Navigation]);

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss']
})
export class SwiperComponent implements OnInit{

  @Input() slides:MainSlideTypes[] | null = null
  @Input() slideTemplate:TemplateRef<HTMLTemplateElement> = null
  @Input() slideControlTpl:TemplateRef<HTMLTemplateElement> = null

  @Output() slideChange: EventEmitter<number> = new EventEmitter();

  navigation = {
    nextEl: '.l',
    prevEl: '.r',
  }

  constructor(
  ) { }

  ngOnInit() {

  }

  onSlideChange(params: EventsParams['activeIndexChange']) {
    const [swiper] = params
    this.slideChange.emit(swiper.realIndex)
  }
}
