import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray} from '@angular/forms';
import { CameraService} from "../../../modules/check/services/camera.service";
import { CommonStore } from "../../../modules/core/services/commonState.service";
import { Subject, Observable, map, tap, switchMap, of, takeUntil  } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import {
  addCurrentNewField,
  saveNewValue,
  editNewValue,
  removeField,
  disableEdit, addCheck
} from "../../../../store/common/actions/common.actions";
import { AppDbService } from "../../../modules/core/services/localStore.service";
import { CheckState, CurrentFields } from "../../../../store/common/types/interfaces";
import { CommonService } from "../../../modules/core/services/common.service";
import {cloneDeep, merge} from 'lodash'
@Component({
  selector: 'check-list-component',
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.scss']
})

export class CheckListComponent implements OnInit{

  formTitle: FormGroup
  formFields: FormGroup
  cancelDisable: Boolean = false
  fieldsInit$: Observable<FormGroup[]>
  checksInit$: Observable<CheckState[]>
  currentImageNotifier$ = new Subject<void>()
  btnEventObservable$: Observable<string>
  titleInit$: Observable<string>
  idInit$: Observable<string>
  addControlsDisable: boolean = false
  editControlsDisable: boolean = false
  isDisableAll: boolean = false
  currentList: CurrentFields[]
  currentId: null | string
  idNotifier = new Subject()
  fieldsNotifier = new Subject()
  btnNotifier = new Subject<void>()
  currentItem: CheckState| null = null
  formFieldsModel = {
    date: '',
    id: '',
    listItems: [] as CurrentFields[],
    title: ''
  }
  currentImageId: number | null = null

  @Output() webcamOnOpen: EventEmitter<boolean> = new EventEmitter();

  constructor(
    // public checkService: CheckService,
    public commonStore$: CommonStore,
    private formBuilder: FormBuilder,
    public cameraService: CameraService,
    public commonService: CommonService,
    public DB$: AppDbService
  ) {

  }

  ngOnDestroy(){
    this.idNotifier.next(null)
    this.idNotifier.complete()
    this.fieldsNotifier.next(null)
    this.fieldsNotifier.complete()
    this.btnNotifier.next()
    this.btnNotifier.complete()
    this.currentImageNotifier$.next()
    this.currentImageNotifier$.complete()
    // this.commonService.btnControls$.unsubscribe()
    // this.commonService.unsubscribe()
  }

  ngOnInit() {
    this.bindSubscribes()
    this.setSubscribes()
    this.bindEvents()
  }

  setSubscribes(){
    this.btnEventObservable$.pipe(
      takeUntil(this.btnNotifier)
    ).subscribe((key) => {
      // console.log(key, 'btn-event')
      switch (key) {
        case 'save': {
          this.saveAll()
          break;
        }
        case 'add': {
          this.addNewField()
          break;
        }
        default: {
          break;
        }
      }
    })
    this.titleInit$
      .subscribe((title) => {
        this.formTitle = this.formBuilder.group({
          listName: [
            title,
            [
              Validators.required
            ]
          ],
        })
        this.commonService.fieldTitleStatus$.next(this.formTitle.valid)
      })
    this.idInit$
      .pipe(
        takeUntil(this.idNotifier),
        switchMap((v) => (v) ? of(v) : of(`id_${new Date().getTime()}`))
      )
      .subscribe((id) => {
        this.currentId = id
        console.log(this.currentId)
    })

    this.fieldsInit$
      .pipe(takeUntil(this.fieldsNotifier))
      .subscribe((forms:FormGroup[]) => {
      // this.addControlsDisable = !this.addControlsDisable
      this.formFields = this.formBuilder.group({
        listItems: this.formBuilder.array(
          forms
        )
      })
      this.commonService.fieldsStatus$.next(this.formFields.valid)
    })

    this.commonService.currentImage$
      .pipe(
        takeUntil(this.currentImageNotifier$)
      )
      .subscribe((currentImageUrl) => {
        // console.log(currentImageUrl, this.currentItem, this.currentImageId, 'currentImageUrl')
        try{
          const itemImage = this.currentItem.listItems.map((listItem) => {
            if (listItem.id === this.currentImageId) {
              listItem = {
                ...listItem,
                imageUrl: currentImageUrl
              }
            }
            return listItem
          })
          console.log(this.currentItem, 'this.currentItem')
          this.currentList = itemImage
          this.saveAll()
        } catch (e) { console.warn(e)}
      })
  }

  inputField(){
    this.commonService.fieldsStatus$.next(this.formFields.valid)
  }

  inputTitleField(){
    this.commonService.fieldTitleStatus$.next(this.formTitle.valid)
  }

  bindSubscribes (){
    this.btnEventObservable$ = this.commonService.btnControls$
    this.titleInit$ = this.commonStore$.currentTitle$
    this.idInit$ = this.commonStore$.currentId$
    this.fieldsInit$ = this.commonStore$.currentCheckFields$.pipe(
      tap((v) => {
        this.currentList = v
        this.setCurrentItem()
      }),
      map((field) => {
        const list = (this.formFields) ? this.formFields.value.listItems : (field || this.formFieldsModel.listItems)
        return list.map((v: { checkPriceName: string, checkPriceValue: number }) => {
          return this.formBuilder.group({
            checkPriceName: [
              v.checkPriceName,
              [
                Validators.required,
                // Validators.maxLength(10)
              ]
            ],
            checkPriceValue: [v.checkPriceValue, [
              Validators.required,
              // Validators.pattern(/^\8\9[0-9]*$/)
            ]]
          });
        })
      })
    )
  }

  initNewField() {

  }

  addNewField() {
    if(!this.formFields) return
    const controls = <FormArray>this.formFields.controls['listItems'];
    const newFieldValue = this.formBuilder.group({
      checkPriceName: ['', [
        Validators.required
      ]],
      checkPriceValue: [null, [
        Validators.required,
        // Validators.pattern(/^\8\9[0-9]*$/)
      ]]
    });
    controls.push(newFieldValue)
    // this.isDisableAll = true
    this.commonStore$.store.dispatch(disableEdit())
    // this.editControlsDisable = true
  }

  updateFieldValue(id:number, formItem: FormArray){
    const currentImageUrl = this.currentList.find(item => item.id === id)
    const newValue = {
      ...formItem.value,
      id:id,
      imageUrl: (currentImageUrl && currentImageUrl.imageUrl) ? currentImageUrl.imageUrl : '',
      isEdit: false,
    }
    this.commonStore$.store.dispatch(addCurrentNewField({ value: newValue }))
    this.commonService.storeNextTrigger('checks', this.currentItem)
  }

  removeField(i:number){
    const controls = <FormArray>this.formFields.controls['listItems'];
    controls.removeAt(i)
    this.commonStore$.store.dispatch(removeField({ value: i }))
    this.commonService.storeNextTrigger('checks', this.currentItem)
  }

  setCurrentItem(){
    this.currentItem = {
      date: new Date().toISOString(),
      id: this.currentId,
      listItems: this.currentList,
      title: this.formTitle.value.listName
    }
  }

  bindEvents() {
    for (const key in this.formFields.controls) {
      if (this.formFields.controls[key]){
        this.formFields.controls[key].valueChanges.subscribe((value) => {
          if (value) {
            this.cancelDisable = false;
          }
        });
      }
    }
  }

  editFieldValue(i:number, formItem?:FormArray | any){
    this.commonStore$.store.dispatch(editNewValue({ value: i }))
    if (formItem) {
      this.updateFieldValue(i, formItem)
    }
  }

  setCheckImage(index:number){
    // this.webcamOnOpen.emit(true)
    const selectedImage = this.currentList.find( item => item.id === index)
    this.currentImageId = index
    this.commonService.onOpenWebcamModal$.next(index)
    if(selectedImage.imageUrl){
      this.cameraService.currentImage = selectedImage.imageUrl
    } else {
      this.cameraService.currentImage = ''
    }
  }

  saveAll(imageUrl?:string){
    // this.commonStore$.store.dispatch(disableEdit())
    if (!this.formTitle || this.formTitle.value.listName === '') return
    this.editControlsDisable = false
    if (this.formFields.valid) {
      const validItemsList = (this.currentList.length === 0) ? this.formFields.value.listItems : merge(
        cloneDeep(this.currentList),
        cloneDeep(this.formFields.value.listItems))
      // const validItemsList = this.formFields.value.listItems
      this.commonStore$.store.dispatch(saveNewValue({ value: validItemsList }))
      this.commonStore$.store.dispatch(disableEdit())
      this.setCurrentItem()
      this.commonStore$.store.dispatch(addCheck({
        value: this.currentItem
      }))
      this.commonService.storeNextTrigger('checks', this.currentItem)
    }
  }
}
