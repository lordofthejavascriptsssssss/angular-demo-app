export interface MainSlideTypes {
  id: number,
  title: string,
  icon: string,
  url: string
}
