import { Pipe, PipeTransform } from '@angular/core';
import { CurrentFields } from "../../../../store/common/types/interfaces";

@Pipe({
  name: 'isSavedItem'
})

export class isSavedItem implements PipeTransform {
  transform(value: CurrentFields[], i:number) {
    return !!value.find((v) => v.id === i)
  }
}
