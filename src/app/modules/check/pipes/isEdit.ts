import { Pipe, PipeTransform } from '@angular/core';
import { CurrentFields } from "../../../../store/common/types/interfaces";

@Pipe({
  name: 'isEdit'
})

export class isEdit implements PipeTransform {
  transform(value: CurrentFields[], i:number) {
    const result = value.find((v) => v.id === i)
    return (result) ? result.isEdit : true
  }
}
