import { Pipe, PipeTransform } from '@angular/core';
import { FormArray, FormGroup } from "@angular/forms";

@Pipe({
  name: 'GetFormsFields'
})

export class GetFormsFields implements PipeTransform {
  transform(value: FormGroup) {
    const controls = <FormArray>value.controls['listItems']
    return controls.controls
  }
}
