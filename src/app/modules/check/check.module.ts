import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckComponent } from "./components/check.component";
import { RouterModule } from "@angular/router";
import { WebcamModule } from 'ngx-webcam';
import { CheckListComponent} from "../../shared/components/check-list/check-list.component";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetFormsFields } from "./pipes/formFieldsList";
import { isSavedItem } from "./pipes/isSavedItem";
import { isEdit } from "./pipes/isEdit";
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { WebcamComponent } from "./components/webcam-component/webcam.component";
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const Router = RouterModule.forChild([
  {
    path: '',
    component: CheckComponent,
  }
]);

@NgModule({
  declarations: [
    CheckComponent,
    CheckListComponent,
    GetFormsFields,
    isSavedItem,
    isEdit,
    WebcamComponent,
  ],
  imports: [
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    WebcamModule,
    Router,
    CommonModule
  ]
})
export class CheckModule { }
