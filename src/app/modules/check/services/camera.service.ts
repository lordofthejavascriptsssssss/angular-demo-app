import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
// import {filter, Observable} from 'rxjs';
// import { Router, NavigationStart } from '@angular/router';
import { CommonService } from "../../core/services/common.service";

@Injectable({
  providedIn: 'root'
})

export class CameraService {

  public showWebcam = true
  public allowCameraSwitch = true
  public multipleWebcamsAvailable = false
  public deviceId: string
  public videoOptions: MediaTrackConstraints = {
    // width: {ideal: 1024},
    // height: {ideal: 576}
  }
  public errors: WebcamInitError[] = []
  public webcamImage: WebcamImage = null
  public trigger$: Subject<void | null> = new Subject()
  public nextWebcam$: Subject<string | boolean> = new Subject()
  public onInit = false
  public currentImage: string = null

  constructor(
    private commonService: CommonService
  ) {
  }


  init(): void {
    this.showWebcam = true
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });
  }

  triggerSnapshot(): void {
    this.trigger$.next(null);
  }

  toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  showNextWebcam(directionOrDeviceId: boolean|string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    console.log(directionOrDeviceId, this.nextWebcam$.asObservable())
    this.nextWebcam$.next(directionOrDeviceId);
  }

  handleImage(webcamImage: WebcamImage): void {
    //  console.info('received webcam image', webcamImage.imageAsDataUrl, this.currentItem);
    this.commonService.setCurrentImage(webcamImage.imageAsDataUrl)
    this.currentImage = webcamImage.imageAsDataUrl
    this.webcamImage = webcamImage;
  }

  cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
    this.onInit = true
  }
}
