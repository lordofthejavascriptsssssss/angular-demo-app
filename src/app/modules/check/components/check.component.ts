import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { CheckService } from "../services/check.service";
import { CameraService } from "../services/camera.service";
import {Subject, Observable, takeUntil, forkJoin,} from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { CommonStore } from "../../core/services/commonState.service";
import { AppDbService } from "../../core/services/localStore.service";
import { CommonService } from "../../core/services/common.service";

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WebcamComponent } from "./webcam-component/webcam.component";

@Component({
  selector: 'app-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.scss']
})
export class CheckComponent implements OnInit {

  storeNotifier$ = new Subject()
  webcamNotifier$ = new Subject<void>()
  constructor(
    // public checkService: CheckService,
    public commonStore: CommonStore,
    public cameraService: CameraService,
    public commonService: CommonService,
    public dialog: MatDialog
    // public dbService$: AppDbService
  ) {
  }

  ngOnInit() {

    this.commonService.onOpenWebcamModal$
      .pipe(
        takeUntil(this.webcamNotifier$)
      )
      .subscribe((v) => {
        this.dialog.open(WebcamComponent)
    })
  }

  ngOnDestroy(){
    // console.log('destroy')
    this.storeNotifier$.next(null)
    this.storeNotifier$.complete()
    this.webcamNotifier$.next()
    this.webcamNotifier$.complete()
  }

  bindSubscribes(){
    this.commonStore.allChecks$
      .pipe(
        takeUntil(this.storeNotifier$)
        // forkJoin({
        //   id:
        // })
      )
      .subscribe((v)=>{
        if(v.length !== 0) {
          // this.dbService$.bulkAdd(v)
        }
    })
  }

  public get triggerObservable(): Observable<void> {
    return this.cameraService.trigger$.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.cameraService.nextWebcam$.asObservable();
  }

}
