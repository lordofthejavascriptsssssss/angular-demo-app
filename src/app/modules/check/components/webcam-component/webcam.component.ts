import { Component, Inject, ViewChild, Input, OnInit, TemplateRef, ElementRef } from '@angular/core';
// import { CheckService } from "../../services/check";
import { CameraService } from "../../services/camera.service";
import { Subject, Observable, takeUntil, forkJoin } from 'rxjs';
// import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
// import { CommonStore } from "../../../core/services/commonState.service";
// import { AppDbService } from "../../../core/services/localStore.service";
// import { CommonService } from "../../../core/services/common.service";
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { MainSlideTypes } from "../../../../shared/types/swiper.types";
import { MatSlideToggleChange } from "@angular/material/slide-toggle";
@Component({
  selector: 'webcam-component',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})

export class WebcamComponent implements OnInit {

  @Input() onOpenModal:Observable<number>
  @ViewChild('webcam') webcamBox: ElementRef<HTMLInputElement>;
  webcamNotifier$ = new Subject<void>()
  isWebcamOpen:boolean = true
  isLoadWebcam = false

  constructor(
    // public checkService: CheckService,
    // public commonStore: CommonStore,
    public cameraService: CameraService,
    // public commonService: CommonService
    // public dbService$: AppDbService
    // public dialogRef: MatDialogRef<any>,
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    console.log(this.webcamBox.nativeElement.clientWidth)
  }

  ngOnDestroy(){
    this.webcamNotifier$.next()
    this.webcamNotifier$.complete()
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  changeLayout(event:MatSlideToggleChange){
    console.log(event)
    this.isWebcamOpen = event.checked
  }

  public get triggerObservable(): Observable<void> {
    return this.cameraService.trigger$.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.cameraService.nextWebcam$.asObservable();
  }

}

