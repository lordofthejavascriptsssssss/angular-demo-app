import { Injectable } from '@angular/core';
// import {filter, Observable} from 'rxjs';
// import { Router, NavigationStart } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class MainService {
  slides = [
    {
      id: 0,
      title: 'Добавить новый чек',
      icon: 'add_circle',
      url: '/check'
    },
    {
      id: 1,
      title: 'Продолжить последний чек',
      icon: 'add_circle',
      url: '/update'
    },
    {
      id: 2,
      title: 'История чеков',
      icon: 'manage_search',
      url: '/history'
    }
  ];
  constructor(
    // private router: Router,
    // private httpService: HttpService,
    // private store: Store,
  ) {
    // this.allServices$ = this.store.pipe(select(getServicesSelector));
    // this.allUsers$ = this.store.pipe(select(getUsersSelector));
    // this.selectUser$ = this.store.pipe(select(getCurrentUserSelector));
  }
}
