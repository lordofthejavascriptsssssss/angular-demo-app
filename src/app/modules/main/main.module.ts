import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from "./components/main.component";
import { RouterModule } from "@angular/router";
import { SwiperModule } from 'swiper/angular';
import { SwiperComponent } from "../../shared/components/swiper/swiper.component";
import { MatIconModule } from '@angular/material/icon';

const Router = RouterModule.forChild([
  {
    path: '',
    component: MainComponent,
  }
]);

@NgModule({
  declarations: [
    MainComponent,
    SwiperComponent
  ],
  imports: [
    Router,
    SwiperModule,
    MatIconModule,
    CommonModule
  ],
  exports: [
    // Moment
  ],
  bootstrap: [
    MainComponent
  ]
})
export class MainModule { }
