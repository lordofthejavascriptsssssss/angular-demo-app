import {ChangeDetectorRef, Component, Input, OnInit, TemplateRef} from '@angular/core';
import { MainService } from "../services/main";
import { HttpService } from "../../core/services/http.service";
import { Router } from '@angular/router';
import { CommonStore } from "../../core/services/commonState.service";
import { clearCurrentState, setCurrentId } from "../../../../store/common/actions/common.actions";
import { CommonService } from "../../core/services/common.service";
import { Output, EventEmitter } from "@angular/core";
import { MainSlideTypes } from "../../../shared/types/swiper.types";
import { Observable , Subject, map } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit{

  slideTitleObservable = new Subject<void>()
  componentIsLoaded: boolean = false
  slideTitle: string = ''
  test: number = 0

  constructor(
    private cd: ChangeDetectorRef,
    private route: Router,
    public mainService: MainService,
    public http: HttpService,
    public commonStore$: CommonStore,
    public commonService: CommonService
  ) {
  }

  routerPush(page:string){
    console.log(page)
    if(page === '/update'){
      this.commonService.next('current')
    }
    this.route.navigate([page]);
  }

  sliderTitleEmit(slide:number) {
    this.slideTitle = this.mainService.slides.find((v) => v.id === slide).title
    this.cd.detectChanges()
    // console.log(this.slideTitle)
  }

  ngOnInit(){
    this.componentIsLoaded = true
  }

  ngAfterViewInit(){
    // this.slideTitle = 'new'
  }

  ngOnDestroy(){

  }
}
