import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { select, Store } from "@ngrx/store";
import { getAllChecks,
  getCommonCount,
  getCommonCurrentCheck,
  getCurrentId,
  getCurrentTitle
} from "../../../../store/common/seloctors/common.selector";
// import {filter, Observable} from 'rxjs';
// import { Router, NavigationStart } from '@angular/router';
import { CheckState, CommonState, CurrentFields } from "../../../../store/common/types/interfaces";
import { clearCurrentState } from "../../../../store/common/actions/common.actions";
@Injectable({
  providedIn: 'root'
})

export class CommonStore {
  isLoaded = false;
  common$: Observable<number>;
  currentCheckFields$: Observable<CurrentFields[]>
  allChecks$: Observable<CheckState[]>
  currentId$: Observable<string>
  currentTitle$: Observable<string>
  constructor(
    // private router: Router,
    // private httpService: HttpService,
    // private store: Store,
    public store: Store<{ common: CommonState }>,
  ) {
    this.common$ = store.pipe(select(getCommonCount))
    this.currentCheckFields$ = store.pipe(select(getCommonCurrentCheck))
    this.allChecks$ = store.pipe(select(getAllChecks))
    this.currentId$ = store.pipe(select(getCurrentId))
    this.currentTitle$ = store.pipe(select(getCurrentTitle))
    // this.allServices$ = this.store.pipe(select(getServicesSelector));
    // this.allUsers$ = this.store.pipe(select(getUsersSelector));
    // this.selectUser$ = this.store.pipe(select(getCurrentUserSelector));
  }
}
