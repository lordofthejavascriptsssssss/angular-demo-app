import { Injectable } from '@angular/core';
import { Router, Event, RouterEvent, NavigationEnd} from "@angular/router";
import { filter } from "rxjs";
import { clearCurrentState, setCurrentId } from "../../../../store/common/actions/common.actions";
import { CommonService } from "./common.service";
import { CommonStore } from "./commonState.service";

@Injectable({
  providedIn: 'root'
})

export class RouterService {

  currentUrl: string = ''
  btnControlsDisabled: boolean = false

  constructor(
    public commonService: CommonService,
    public router: Router,
    public commonStore$:CommonStore
  ) {
  }

  bindRouterSubscribe(){
    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd)
      )
      .subscribe((routerEvent) => {
        const url = this.currentUrl = Object.assign(routerEvent).url
        switch (url) {
          case '/update':
            // this.commonStore$.store.dispatch(setCurrentId(null))
            {
              this.btnControlsDisabled = true
              // this.commonService.next('current')
            }
            break;
          case '/check':
          {
            this.btnControlsDisabled = true
            this.commonStore$.store.dispatch(setCurrentId(null))
            this.commonStore$.store.dispatch(clearCurrentState())
          }
            break;
          default:
            this.btnControlsDisabled = false
            break;
        }
    })
  }

}
