import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Observable, forkJoin, of, delay, combineLatest, switchMap, pipe, map, zip} from "rxjs";
import { utilsMap } from "../utils/indexedDb.util";
import { CheckState, CurrentFields } from "../../../../store/common/types/interfaces";
interface StorageData {
  current: CheckState | null,
  checks: CheckState[] | null
}

@Injectable({
  providedIn: 'root'
})


export class AppDbService {

  constructor(
    // private appDbService: NgxIndexedDBService
  ) {

  }

  getObservable(actionType: string, storageName:string, newValue?:string):Observable<string | null>{
    return new Observable((s) => {
      if(!window.localStorage) return null
      const store = window.localStorage
      switch (actionType) {
        case 'get':
          try{
            (store.getItem(storageName)) ? s.next(store.getItem(storageName)) : s.next(null)
          } catch (e) { console.error(e); s.next(null) }
          break;
        case 'set':
          try{
            store.setItem(storageName, newValue);
            (store.getItem(storageName)) ? s.next(store.getItem(storageName)) : s.next(null)
          } catch (e) { console.error(e); s.next(null) }
          break;
      }
    })
  }

  getAll():Observable<StorageData> {
    return zip(
      this.getObservable('get', utilsMap.current),
      this.getObservable('get', utilsMap.checks)).pipe(
      map(([current, checks]) => {
        return {
          current: JSON.parse(current)?.value,
          checks: JSON.parse(checks)
        }
      })
    )
  }

  set(storageName:string, items:CheckState[]) {
    return this.getObservable('set', storageName, JSON.stringify(items))
  }

  get(storageName:string) {
    return this.getObservable('get', storageName)
    // return this.getObservable('get', storageName)
  }

}
