import { Injectable } from '@angular/core';
import {
  Subject,
  switchMap,
  takeUntil,
  of,
  map,
  zip,
  BehaviorSubject,
} from "rxjs";
import { CommonStore } from "./commonState.service";
import { AppDbService } from "./localStore.service";
import { CheckState, CurrentFields} from "../../../../store/common/types/interfaces";
import { utilsMap } from "../utils/indexedDb.util";
import { addCheck, clearCurrentState, setCurrent } from "../../../../store/common/actions/common.actions";

interface StoreInterface {

  type:string,
  value: CheckState[] | CurrentFields

}

@Injectable({
  providedIn: 'root'
})

export class CommonService {

  update$ = new Subject()
  store$ = new Subject()
  dbNotifier$ = new Subject<void>()
  storeNotifier$ = new Subject<void>()
  updateNotifier$ = new Subject<void>()
  btnControls$ = new BehaviorSubject<string>('')
  fieldsStatus$ = new BehaviorSubject<boolean>(false)
  fieldTitleStatus$ = new BehaviorSubject<boolean>(false)
  onOpenWebcamModal$ = new Subject<number | null>()
  currentImage$ = new Subject<string | null>()
  checkTitle: string = ''
  current: CheckState | null = null
  // addBtnDisable: boolean = false

  constructor(
    // private router: Router,
    // private httpService: HttpService,
    // private store: Store,
    public commonStore$: CommonStore,
    public dbService$: AppDbService

  ) {
    // this.allServices$ = this.store.pipe(select(getServicesSelector));
    // this.allUsers$ = this.store.pipe(select(getUsersSelector));
    // this.selectUser$ = this.store.pipe(select(getCurrentUserSelector));
  }

  btnControlNext(key:string){
    this.btnControls$.next(key)
  }

  setCurrentImage(imageUrl:string){
    this.currentImage$.next(imageUrl);
  }

  currentUpdate(){
    this.update$.next(utilsMap.current)
  }

  bindSubscribes(){
    // this.btnControls$.pipe()
    this.store$
      .pipe(
        takeUntil(this.storeNotifier$),
        switchMap((v: StoreInterface | any) => {
          return zip(of(v),
            this.commonStore$.allChecks$,
          ).pipe(map(([item, checks]) => ({ item, checks})))
        }),
      )
      .subscribe({
          next: ({item, checks}) => {
            if (checks) this.dbService$.set(utilsMap.checks, checks).subscribe()
            if (Object.keys(item).length !== 0 && item){
              this.dbService$.set(utilsMap.current, item).subscribe()
            }
          }
      })
    this.update$
      .pipe(
        takeUntil(this.updateNotifier$),
        switchMap((v) => {
          if (v === 'all') { return this.dbService$.getAll() }
          if (v === 'checks') { return this.dbService$.get(utilsMap.checks).pipe(map(checks =>  {
            return {
              current: null,
              checks: JSON.parse(checks)
            }
          })) }
          if (v === 'current') { return this.dbService$.get(utilsMap.current).pipe(map(current =>  {
            return {
              current: JSON.parse(current),
              checks: null
            }
          })) }
          return this.dbService$.getAll()
        })
      )
      .subscribe({
        next: (result) => {
          console.warn(result, 'result')
          if (
            typeof result !== 'string'
            && result.checks
          ) { this.commonStore$.store.dispatch(addCheck({ value: result.checks })) }
          if (typeof result !== 'string'
            && result.current) {
            this.commonStore$.store.dispatch(setCurrent({ value: (result.current.value) ? result.current.value : result.current }))
          }
        }
      });
  }

  storeNextTrigger(key:string, item: CheckState){
    this.store$.next({
      type: utilsMap.checks,
      value: item ? item : null
    })
  }

  unsubscribe(){
    this.dbNotifier$.next()
    this.dbNotifier$.complete()
    this.storeNotifier$.next()
    this.storeNotifier$.complete()
    this.updateNotifier$.next()
    this.updateNotifier$.complete()
  }

  next(key:string){
    this.update$.next(key)
  }
}
