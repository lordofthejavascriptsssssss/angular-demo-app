import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../main/main.module').then(mod => mod.MainModule)
  },
  {
    path: 'history',
    loadChildren: () => import('../history/history.module').then(mod => mod.HistoryModule)
  },
  {
    path: 'check',
    loadChildren: () => import('../check/check.module').then(mod => mod.CheckModule)
  },
  {
    path: 'update',
    loadChildren: () => import('../check/check.module').then(mod => mod.CheckModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
