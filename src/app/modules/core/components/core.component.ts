import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CommonService } from "../services/common.service";
import { Observable, pipe, Subject, takeUntil, map, switchMap } from "rxjs";
import { Store, select } from "@ngrx/store";
import { getCommonCount } from "../../../../store/common/seloctors/common.selector";
import { RouterService } from "../services/router.service";
// import { CommonStore } from "../services/common.state";
// import { AppDbService } from "../services/indexedDB.state";
// import { NgxIndexedDBService } from 'ngx-indexed-db';
@Component({
  selector: 'app-root',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit {

  addBtnDisable:boolean = false
  setTitle:boolean = false
  appIsStart:boolean = false

  constructor(
    public commonService: CommonService,
    public routerService: RouterService,
    private cd: ChangeDetectorRef,
  ) {
  }

  ngOnInit() {
    this.commonService.bindSubscribes()
    this.commonService.next('all')
    this.routerService.bindRouterSubscribe()
    this.commonService.fieldsStatus$.subscribe((v) => {
      this.addBtnDisable = v
      this.cd.detectChanges()
    })
    this.commonService.fieldTitleStatus$.subscribe((v) => {
      this.setTitle = v
      this.cd.detectChanges()
    })
  }

  setStartApp(key:boolean){
    this.appIsStart = key
  }

  ngOnDestroy(){
    this.commonService.fieldsStatus$.unsubscribe()
    //this.commonService.fieldTitleStatus$.unsubscribe()
  }
}
