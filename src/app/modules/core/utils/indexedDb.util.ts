export const utilsMap = {
  checks: 'checks',
  current: 'currentCheck',
}

export const dbConfig = {
  name: 'appDb',
  version: 1,
  objectStoresMeta: [
    {
      store: 'currentCheck',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        {name: 'title', keypath: 'title', options: {unique: false}},
        {name: 'id', keypath: 'id', options: {unique: false}},
        {name: 'list', keypath: 'list', options: {unique: false}},
      ]
    },
    {
      store: 'checks',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        {name: 'date', keypath: 'date', options: {unique: false}},
        {name: 'id', keypath: 'id', options: {unique: false}},
        {name: 'title', keypath: 'title', options: {unique: false}},
        {name: 'listItems', keypath: 'listItems', options: {unique: false}}
      ]
    }]
};
