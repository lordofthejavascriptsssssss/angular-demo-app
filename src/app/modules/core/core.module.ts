import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from "./components/core.component";
import { BrowserModule } from "@angular/platform-browser";
import { CoreRoutingModule } from "./core-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatIconModule } from '@angular/material/icon';
import {
  HTTP_INTERCEPTORS,
  HttpClientModule
} from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { commonReducer } from "../../../store/common/common.reducers";
import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';
import {CurrentFields} from "../../../store/common/types/interfaces";
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { dbConfig } from "./utils/indexedDb.util";

const Config: DBConfig = dbConfig

@NgModule({
  declarations: [
    CoreComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forRoot({
      common: commonReducer
    }),
    NgxIndexedDBModule.forRoot(Config),
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    CoreRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    MatIconModule,
    HttpClientModule,
    NgbModule
  ],
  bootstrap: [
    CoreComponent
  ]
})
export class CoreModule { }
