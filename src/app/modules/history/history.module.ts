import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryComponent } from "./components/history.component";
import { RouterModule } from "@angular/router";
import { MatCardModule } from '@angular/material/card';
import { Moment } from "./pipes/moment";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from '@angular/material/button';
// import { MomentModule } from 'ngx-moment';
const Router = RouterModule.forChild([
  {
    path: '',
    component: HistoryComponent,
  }
]);

@NgModule({
  declarations: [
    Moment,
    HistoryComponent
  ],
  imports: [
    MatButtonModule,
    MatIconModule,
    CommonModule,
    MatCardModule,
    Router,
    // MomentModule
  ]
})
export class HistoryModule { }
