import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../core/services/common.service";
import { map, Observable, takeUntil, tap, Subject } from "rxjs";
import { CheckState } from "../../../../store/common/types/interfaces";
import { Validators } from "@angular/forms";
import { CommonStore } from "../../core/services/commonState.service";
import { removeCheck, setCurrent } from "../../../../store/common/actions/common.actions";
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  historyChecksList$: Observable<CheckState[]>

  constructor(
    public commonService: CommonService,
    public commonStore$: CommonStore,
    private route: Router,
  ) {
  }

  ngOnInit(){
    this.commonService.next('checks')
    this.bindSubscribes()
  }

  bindSubscribes (){
    this.historyChecksList$ = this.commonStore$.allChecks$
  }

  setCurrent(item:CheckState){
    this.commonStore$.store.dispatch(setCurrent({ value:item }))
    // this.commonService.storeNextTrigger('checks', null)
    this.route.navigate(['/update']);
  }

  removeCheckItem(index:string){
    this.commonStore$.store.dispatch(removeCheck({ value:index }))
    this.commonService.storeNextTrigger('checks', null)
  }

  ngOnDestroy(){
  }
}
