import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'Moment'
})

export class Moment implements PipeTransform {
  transform(value: string) {
    return moment(value).subtract(10, 'days').calendar()
  }
}
